function sendResponse(promise, res) {
    console.log('sendResponse');
    promise
        .then(result => {
            console.log('sendResponse.then(result): ' + result);
            res.status(200).json(result);
        })
        .catch(err => {
            console.log('sendResponse.catch(err): ' + err);
            res.status(200).json({ error: err });
        })
}

module.exports.sendResponse = sendResponse;