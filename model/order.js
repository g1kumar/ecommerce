const mongoose = require('mongoose');
const Product = require('./product')

const orderSchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    products: [{
        _id : mongoose.Types.ObjectId,
        title : String,
        price: Number,
        quantity : Number,
    }],
    status: String,
    user : String
})

module.exports = mongoose.model('Order', orderSchema)
