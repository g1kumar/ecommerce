const mongoose = require('mongoose');

const productSchema = mongoose.Schema({
    _id: mongoose.Types.ObjectId,
    title: String,
    price: Number,
    user: String  
})

module.exports = mongoose.model('Product', productSchema)