const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    if (req.header('Authorization')) {
        try {
            const decoded = jwt.verify(req.header('Authorization'), process.env.JWT_SECRET);
            req.userData = decoded;
            next();
        } catch (err) {
            res.status(500).json({ error: err });
        }
    } else {
        res.status(500).json({ error: 'Access denied' });
    }
}