const express = require('express');
const router = express.Router();
const User = require('../../model/user');
const common = require('../../common');
const uuid = require('uuid');
const mongoose = require('mongoose');
const sendResponse = common.sendResponse;
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

router.post('/register', (req, res, next) => {
    User.find({ email: req.body.email }).then(d => {
        console.log("User.find: " + d);
        if (d != null && d.length > 0) {
            console.log("User found ");
            res.status(500).json({ error: "email already exists" });
        } else {
            console.log("User not found ");
            bcrypt.hash(req.body.password, 10, (err, hash) => {
                console.log("bcrypt.hash().err: " + err);
                console.log("bcrypt.hash().hash: " + hash);
                if (err) {
                    res.status(500).json({ error: err });
                } else if (hash) {
                    let user = new User({
                        _id: mongoose.Types.ObjectId(),
                        email: req.body.email,
                        password: hash
                    });
                    user.save()
                        .then(d => {
                            res.status(200).json({message: "User successfully registered"});
                        })
                        .catch(err => {
                            res.status(500).json({ error: err });
                        })
                }
            });
        }
    }).catch(err => {
        res.status(500).json({ error: err });
    })

})

router.post('/login', (req, res, next) => {
    User.findOne({ email: req.body.email }).then(u => {
        if (u != null) {
            bcrypt.compare(req.body.password, u.password, (err, same) => {
                console.log("bcrypt.compare.err: " + err);
                console.log("bcrypt.compare.same: " + same);
                if (err) {
                    res.status(500).json({ error: err });
                } else if (same) { 
                    console.log("process.env + " + process.env);
                    console.log("process.env.MONGO_ATLAS_USERNAME" + process.env.MONGO_ATLAS_USERNAME);
                    console.log("process.env.JWT_SECRET + " + process.env.JWT_SECRET);
                    let token = jwt.sign({email: u.email, userId: u._id}, process.env.JWT_SECRET)
                    res.status(200).json({ email: u.email, token: token });
                } else {
                    res.status(200).json({ error: "Incorrect password" });
                }
            })
            // res.status(200).json(u);
        } else {
            res.status(200).json({ error: "Email not found" });
        }
    }).catch(err => {
        res.status(500).json({ error: err });
    })
})

router.post('/updatepassword', (req, res, next) => {
    if (req.body.email) {
        User.find({ email: req.body.email }).then(d => {
            d.password = req.body.password;
            sendResponse(User.updateOne({ _id: d._id }, { $set: d }), res);
        }).catch(err => {
            res.status(500).json({ error: err });
        })
    } else {
        res.status(500).json({ error: "user not registered" });
    }
})

module.exports = router;
